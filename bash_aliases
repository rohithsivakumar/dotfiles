##################################################################

### My stuff

### activate your Python virtual environment from the containing project directory
alias vac="source venv/bin/activate"

### deactivate your Python virtual environment
alias vdc="deactivate"

alias gphm="git push heroku master"

alias gstat="git status"

##################################################################

### taken from Ben Roytenberg's "brilliant-bash" (https://github.com/roytenberg/brilliant-bash)

### sudo hack: so you can use custom aliases as sudo
###
### NOTE - bash will normally stop recognizing aliases after it sees
### the space after the command sudo, but if it sees an alias that
### ends in a space, it will attempt to detect another alias after.
alias sudo="sudo "

### update: update all of your packages!
if [ ! -z "$(which brew)" ]; then
  alias update="brew update && brew upgrade"
elif [ ! -z "$(which pacman)" ]; then
  alias update="sudo pacman -Syyu"
elif [ ! -z "$(which apt)" ]; then
  alias update="sudo apt update && sudo apt upgrade && sudo apt full-upgrade"
elif [ ! -z "$(which apt-get)" ]; then
  alias update ="sudo apt-get update && sudo apt-get upgrade && sudo apt-get dist-upgrade"
elif [ ! -z "$(which dnf)" ]; then
  alias update="sudo dnf upgrade"
elif [ ! -z "$(which yum)" ]; then
  alias update="su -c 'yum update'"
elif [ ! -z "$(which zypper)" ]; then
  alias update="sudo zypper update"
fi

### myip: prints out your IP address. Handy to check if your VPN is on!
alias myip="curl icanhazip.com"

### restart: a quick refresh for your shell instance.
# may need to change this to bash_profile
# alias restart="source ~/.bashrc"

### gpom: simplistic git push origin master alias.
alias gpom="git push origin master"
